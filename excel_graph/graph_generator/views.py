from django.shortcuts import render
from .forms import ExcelUploadForm
from .utils import generate_line_graph

import base64


def upload_excel(request):
    if request.method == 'POST':
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            excel_file = request.FILES['excel_file']
            graph_buffer = generate_line_graph(excel_file)
            graph_base64 = base64.b64encode(graph_buffer.getvalue()).decode('utf-8')
            return render(request, 'graph_generator/result.html', {'graph_base64': graph_base64})
    else:
        form = ExcelUploadForm()

    return render(request, 'graph_generator/upload.html', {'form': form})
