import pandas as pd
import matplotlib
matplotlib.use('Agg')  # Add this line

import matplotlib.pyplot as plt
import io

def generate_line_graph(file):
    df = pd.read_excel(file)

    # Parse the date values
    df['Deadline Date'] = pd.to_datetime(df['Deadline Date'], dayfirst=True, errors='coerce')

    # Group the data by IPFamily
    grouped = df.groupby('IPFamily')

    # Generate a line graph for each IPFamily
    for name, group in grouped:
        plt.plot(group['Deadline Date'], group['Deadline Name'], marker='o', label=name)

    # Set labels and title
    plt.xlabel('Deadline Date')
    plt.ylabel('Deadline Name')
    plt.title('Deadlines by IP Family')
    plt.legend()

    # Convert the graph to an image buffer
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plt.close()

    return buffer
